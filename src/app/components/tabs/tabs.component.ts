import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-tabs',
  templateUrl: './tabs.component.html',
  styleUrls: ['./tabs.component.scss']
})
export class TabsComponent implements OnInit {
  clients = [1,2,3,4,5];
  contents = [1, 2, 3,4,5].map((data) => {
    return {
      tab: `Service ${data}`,
      title: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit' + data,
      description: `Lorem ipsum dolor sit amet consectetur adipisicing elit. Cum non incidunt
      aut mollitia neque laboriosam aspernatur? Voluptatem eveniet soluta ${data}`
    }
  });
  constructor() { }

  ngOnInit() {
  }

}
