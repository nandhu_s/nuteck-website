import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-client-logos',
  templateUrl: './client-logos.component.html',
  styleUrls: ['./client-logos.component.scss']
})
export class ClientLogosComponent implements OnInit {
  clients = [1,2,3,4,5]
  constructor() { }

  ngOnInit() {
  }

}
