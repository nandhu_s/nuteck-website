import { Component, OnInit, ViewChild } from '@angular/core';
import { faChevronDown } from '@fortawesome/free-solid-svg-icons';
import { NgbAccordion } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-filter-accordian',
  templateUrl: './filter-accordian.component.html',
  styleUrls: ['./filter-accordian.component.scss']
})
export class FilterAccordianComponent implements OnInit {
  private icons = {
    dropDown: faChevronDown
  }
  filters = ['panel 1','panel 2','panel 3','panel 4','panel 5']
  @ViewChild('acc',{static:true}) accordian:NgbAccordion;
  constructor() { }

  ngOnInit() {
  }

  toggle(id) {
    this.accordian.toggle(id)
  }

}
