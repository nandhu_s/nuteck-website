import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-case-study-small',
  templateUrl: './case-study-small.component.html',
  styleUrls: ['./case-study-small.component.scss']
})
export class CaseStudySmallComponent implements OnInit {
image = `https://picsum.photos/900/500?random&t=${Math.random()}`;
cards = [1,2]
  constructor() { }

  ngOnInit() {
  }

}
