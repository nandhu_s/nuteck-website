import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-service-cards',
  templateUrl: './service-cards.component.html',
  styleUrls: ['./service-cards.component.scss']
})
export class ServiceCardsComponent implements OnInit {
  cards = [{ title: 'Civil', img: '../../../assets/mechnical.svg' }, { title: 'Mechanical', img: '../../../assets/Technology.svg' }, { title: 'Technology', img: '../../../assets/mechnical.svg' }]
  constructor() { }

  ngOnInit() {
  }

}
