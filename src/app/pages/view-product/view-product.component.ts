import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-view-product',
  templateUrl: './view-product.component.html',
  styleUrls: ['./view-product.component.scss']
})
export class ViewProductComponent implements OnInit {
  productImages = [{ id: 1, src: '../../../assets/imageOne.jpg' },
  { id: 2, src: '../../../assets/imageOne.jpg' },
  { id: 3, src: '../../../assets/imageOne.jpg' },
  { id: 4, src: '../../../assets/imageOne.jpg' }]
  selectedImage = this.productImages[0];

  constructor() { }

  ngOnInit() {
  }
  selectProduct(image) {
    this.selectedImage = image;
  }
}
