import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.scss']
})
export class ContactUsComponent implements OnInit {
  private stars = [1, 2, 3, 4];
  private unStars = [1];
  constructor() { }

  ngOnInit() {
  }

}
