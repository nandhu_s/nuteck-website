import { NgModule, Component } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { QualificationCertificateComponent } from './pages/qualification-certificate/qualification-certificate.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { HomeComponent } from './pages/home/home.component';
import { CivilComponent } from './pages/civil/civil.component';
import { CaseStudiesComponent } from './pages/case-studies/case-studies.component';
import { PipingSolutionsComponent } from './pages/piping-solutions/piping-solutions.component';
import { FabricationComponent } from './pages/fabrication/fabrication.component';
import { ViewProductComponent } from './pages/view-product/view-product.component';


const routes: Routes = [{
  path: '',
  component: HomeComponent
}, {
  path: 'civil',
  component: CivilComponent
}, {
  path: 'case-studies',
  component: CaseStudiesComponent
}, {
  path: 'mechanical',
  children: [{
    path: 'piping-solution',
    component: PipingSolutionsComponent,
  },
  {
    path: 'piping-solution/:product',
    component: ViewProductComponent
  },
  {
    path: 'fabrication',
    component: FabricationComponent
  },
  {
    path: 'fabrication/:product',
    component: ViewProductComponent
  },
  ]
}, {
  path: 'company/about-us',
  component: AboutUsComponent
}, {
  path: 'company/qualification-certificate',
  component: QualificationCertificateComponent
},
{
  path: 'company/contact-us',
  component: ContactUsComponent
}];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
