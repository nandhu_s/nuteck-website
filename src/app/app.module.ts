import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './components/header/header.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { FooterComponent } from './components/footer/footer.component';
import { AboutUsComponent } from './pages/about-us/about-us.component';
import { QualificationCertificateComponent } from './pages/qualification-certificate/qualification-certificate.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { HomeComponent } from './pages/home/home.component';
import { CarouselComponent } from './components/carousel/carousel.component';
import { CaseStudySmallComponent } from './components/case-study-small/case-study-small.component';
import { ServiceCardsComponent } from './components/service-cards/service-cards.component';
import { ClientLogosComponent } from './components/client-logos/client-logos.component';
import { CarouselSmallComponent } from './components/carousel-small/carousel-small.component';
import { CivilComponent } from './pages/civil/civil.component';
import { TabsComponent } from './components/tabs/tabs.component';
import { CaseStudiesComponent } from './pages/case-studies/case-studies.component';
import { BannerComponent } from './components/banner/banner.component';
import { ProductsComponent } from './components/products/products.component';
import { PipingSolutionsComponent } from './pages/piping-solutions/piping-solutions.component';
import { SearchComponent } from './components/search/search.component';
import { FilterAccordianComponent } from './components/filter-accordian/filter-accordian.component';
import { ProductItemCardComponent } from './components/product-item-card/product-item-card.component';
import { ViewProductComponent } from './pages/view-product/view-product.component';
import { FabricationComponent } from './pages/fabrication/fabrication.component';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    AboutUsComponent,
    QualificationCertificateComponent,
    ContactUsComponent,
    HomeComponent,
    CarouselComponent,
    CaseStudySmallComponent,
    ServiceCardsComponent,
    ClientLogosComponent,
    CarouselSmallComponent,
    CivilComponent,
    TabsComponent,
    CaseStudiesComponent,
    BannerComponent,
    ProductsComponent,
    PipingSolutionsComponent,
    SearchComponent,
    FilterAccordianComponent,
    ProductItemCardComponent,
    ViewProductComponent,
    FabricationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FontAwesomeModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
